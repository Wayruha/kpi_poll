package edu.kpi.external;

import lombok.Data;

@Data
public class AcademicResponse {
    private String teacher_id;
    private String teacher_name;
    private String teacher_full_name;
    private String teacher_short_name;
}
