package edu.kpi.external;

import lombok.Data;

@Data
public class RozkladOrgMeta {
    private int total_count;
    private int offset;
    private int limit;
}
