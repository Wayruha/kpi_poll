package edu.kpi.external;


import lombok.Data;

import java.util.List;

@Data
public class HttpResponse {
    private String statusCode;
    private String timeStamp;
    private String message;
    private RozkladOrgMeta meta;
    private List<AcademicResponse> data;
}
