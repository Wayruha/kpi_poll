package edu.kpi.external;

import com.squareup.okhttp.*;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Arrays;

@Component
public class HttpClient {

    private static OkHttpClient client;
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public HttpClient() {
        if (client == null) {
            client = new OkHttpClient();
        }
    }


    public String doGetRequest(String url, Pair<String, String>... parameters) throws IOException {

        HttpUrl.Builder httpBuider = HttpUrl.parse(url).newBuilder();
        Arrays.stream(parameters).
                forEach(pair -> httpBuider.addQueryParameter(pair.getKey(), pair.getValue()));

        Request request = new Request.Builder()
                .url(httpBuider.build())
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }


    public String doPostRequest(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

}
