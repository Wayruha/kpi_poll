package edu.kpi.repository;


import edu.kpi.model.entity.Academic;
import edu.kpi.model.entity.Criterion;
import edu.kpi.model.entity.Vote;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@Repository
@Transactional
public interface VoteRep extends CrudRepository<Vote, Long> {

    List<Vote> getByStudentIdAndCriterionAndAcademic(String studentId, Criterion criterion, Academic academic);

    List<Vote> getByAcademic(Academic academic);

    Set<Vote> getByAcademicAndCriterion(Academic academic, Criterion criterion);

    Set<Vote> getByAcademicAndStudentId(Academic academic, String studentId);

    List<Vote> findAll();
}