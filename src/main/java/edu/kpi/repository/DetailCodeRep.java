package edu.kpi.repository;

import edu.kpi.model.entity.Criterion;
import edu.kpi.model.entity.DetailCode;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DetailCodeRep extends CrudRepository<DetailCode, Long> {

    List<DetailCode> getByCriterion(Criterion criterion);
}
