package edu.kpi.repository;

import edu.kpi.model.entity.PollType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PollTypeRep extends CrudRepository<PollType, Long> {
    List<PollType> findAll();

    PollType getByName(String name);

}
