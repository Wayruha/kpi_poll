package edu.kpi.repository;

import edu.kpi.model.dto.AcademicDto;
import edu.kpi.model.entity.Academic;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;


public interface AcademicRep extends PagingAndSortingRepository<Academic, Long> {


    Set<Academic> findAll();

    @Query("select new edu.kpi.model.dto.AcademicDto(ac.academicId,ac.fullName,ac.position,ac.label,ap.average,ap.votesCount) from Academic ac left join ac.averagePoint ap")
    List<AcademicDto> findAcademicsDetails(Pageable pageable);

    @Query("select new edu.kpi.model.dto.AcademicDto(ac.academicId,ac.fullName,ac.position,ac.label,ap.average,ap.votesCount) from Academic ac left join ac.averagePoint ap where lower(ac.fullName) like :query")
    List<AcademicDto> findAcademicsDetails(@Param("query") String query, Pageable pageable);

    @Query("select count(*) from Academic ac left join ac.averagePoint ap where (:query is null OR lower(ac.fullName) like :query) ")
    Integer findAcademicsDetailsCountOnly(@Param("query") String query);

    @Query("select new edu.kpi.model.dto.AcademicDto(ac.academicId,ac.fullName,ac.position,ac.label,ap.average,ap.votesCount) from Academic ac left join ac.averagePoint ap where ac.academicId in :academicIds")
    List<AcademicDto> findAcademicsDetails(@Param("academicIds") List<Long> academicIds, Pageable pageable);

    @Query("select ac FROM Academic ac where ac.sourceId in :sourceIds")
    List<Academic> findAcademicBySourceId(@Param("sourceIds") List<String> sourceIds);

    @Query("select new edu.kpi.model.dto.AcademicDto(ac.academicId,ac.fullName,ac.position,ac.label,ap.average,ap.votesCount) from Academic ac left join ac.averagePoint ap where ac.academicId = :academicId")
    AcademicDto findAcademicDetails(@Param("academicId") Long academicId);

    /*@Query("SELECT ap FROM AveragePoint ap where ap.academic =:academic")
    AveragePoint findAveragePointByAcademic(Academic academic);

    @Query("SELECT ap FROM AveragePoint ap")
    Set<AveragePoint> findAllAcademicAverage();*/

}
