package edu.kpi.repository;

import edu.kpi.model.entity.Criterion;
import edu.kpi.model.entity.PollType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface CriterionRep extends CrudRepository<Criterion, Long> {
    List<Criterion> findAll();

    List<Criterion> getByPollType(PollType pollType);
}
