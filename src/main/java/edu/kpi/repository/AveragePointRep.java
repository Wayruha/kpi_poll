package edu.kpi.repository;

import edu.kpi.model.entity.AveragePoint;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface AveragePointRep extends CrudRepository<AveragePoint, Long> {

    Set<AveragePoint> findAll();
}
