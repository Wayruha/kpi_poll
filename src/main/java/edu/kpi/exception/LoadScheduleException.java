package edu.kpi.exception;

public class LoadScheduleException extends Exception {
    private String groupName;

    public LoadScheduleException(String groupName) {
        this.groupName = groupName;
    }

    public LoadScheduleException(String message, String groupName) {
        super(message);
        this.groupName = groupName;
    }

    public LoadScheduleException(String message, Throwable cause, String groupName) {
        super(message, cause);
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
