package edu.kpi.service;

import edu.kpi.model.entity.Criterion;
import edu.kpi.model.entity.PollType;
import edu.kpi.repository.CriterionRep;
import edu.kpi.repository.PollTypeRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CriterionService {

    @Autowired
    private CriterionRep criterionRep;

    @Autowired
    private PollTypeRep pollTypeRep;

    private static final String ACADEMIC_VOTE_TYPE = "academic voting";

    public List<Criterion> getAcademicCriteria() {

        PollType academicPoll = pollTypeRep.getByName(ACADEMIC_VOTE_TYPE);
        return criterionRep.getByPollType(academicPoll);
    }
}
