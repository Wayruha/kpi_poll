package edu.kpi.service;

import edu.kpi.model.Student;
import edu.kpi.model.dto.AcademicCriterionSummary;
import edu.kpi.model.entity.Academic;
import edu.kpi.model.entity.Criterion;
import edu.kpi.model.entity.DetailCode;
import edu.kpi.model.entity.Vote;
import edu.kpi.repository.CriterionRep;
import edu.kpi.repository.DetailCodeRep;
import edu.kpi.repository.VoteRep;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class VoteServiceImpl implements VoteService {

    @Autowired
    private VoteRep voteRep;

    @Autowired
    private DetailCodeRep detailCodeRep;

    @Autowired
    private CriterionRep criterionRep;

    @Autowired
    private AcademicService academicService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Vote vote(Student stud, Academic academic, Long criterionId, Long dtlCodeId) {
        DetailCode dtlCode = detailCodeRep.findOne(dtlCodeId);
        Criterion criterion = criterionRep.findOne(criterionId);
        return vote(stud, academic, criterion, dtlCode);
    }

    @Override
    public Vote vote(Student stud, Academic academic, Criterion criterion, DetailCode dtlCode) {
        List<Vote> existingVote = voteRep.getByStudentIdAndCriterionAndAcademic(stud.getKpiId(), criterion, academic);
        if (existingVote != null && existingVote.size() > 0) {
            existingVote.forEach(vote->{
                voteRep.delete(vote);
                academicService.updateStatistic(academic, Double.valueOf(vote.getValue().getValue()),false);
            });
        }
        Vote vote = new Vote();
        vote.setAcademic(academic);
        vote.setStudentId(stud.getKpiId());
        vote.setCriterion(criterion);
        vote.setValue(dtlCode);

        vote = voteRep.save(vote);

        String value = Optional.ofNullable(dtlCode.getValue()).orElse("0");
        academicService.updateStatistic(academic, Double.valueOf(value),true);
        return vote;
    }

    @Deprecated
    @Override
    public AcademicCriterionSummary findAverageValueForAcademicAndCriterion(Academic academic, Criterion criterion) {
        //TODO only applicable for some criterion types
        Set<Vote> votes = voteRep.getByAcademicAndCriterion(academic, criterion);
        List<String> values = votes.stream().map(vote -> vote.getValue().getValue()).collect(Collectors.toList());
        double sum = values.stream().mapToDouble(Double::valueOf).sum();
        final int totalVotesCount = values.size();
        final double average = totalVotesCount > 0 ? sum / totalVotesCount : 0;

        AcademicCriterionSummary results = new AcademicCriterionSummary(academic.getAcademicId(), criterion.getCriterionId(), criterion.getDescription());
        results.setAverageValue(average);
        results.setTotalVoteCount(totalVotesCount);
        return results;

    }

    @Override
    public List<AcademicCriterionSummary> getAcademicResults(long academicId) {
        Academic academic = academicService.getById(academicId);
        List<Vote> votes = voteRep.getByAcademic(academic);
        Map<Criterion, List<Vote>> criterionVotesMap = votes.stream().collect(Collectors.groupingBy(Vote::getCriterion));
        List<AcademicCriterionSummary> resultList = criterionVotesMap.entrySet().stream()
                .map(entry -> {
                    Criterion key = entry.getKey();
                    List<Vote> values = entry.getValue();
                    AcademicCriterionSummary summary = new AcademicCriterionSummary(academicId, key.getCriterionId(), key.getDescription());
                    summary.setTotalVoteCount(values.size());
                    double averageValue = values.stream().collect(Collectors.averagingDouble(vote -> Integer.parseInt(vote.getValue().getValue())));
                    summary.setAverageValue(averageValue);
                    return summary;
                }).collect(Collectors.toList());

        return resultList;
    }

    @Override
    public Map<Criterion, DetailCode> getVotesByStudentAndCriteria(Long academicId, Student student, List<Criterion> criteria) {
        Academic academic = new Academic();
        academic.setAcademicId(academicId);
        Set<Vote> votes = voteRep.getByAcademicAndStudentId(academic, student.getKpiId());
        Map<Criterion, DetailCode> resultMap = votes.stream().filter(vote -> criteria.contains(vote.getCriterion())).collect(Collectors.toMap(Vote::getCriterion, Vote::getValue,
                (val1, val2) -> {
                    logger.error("Duplicate Vote (same student,academic,criterion):student:" + student.getKpiId() + ";academic:" + academic.getAcademicId());
                    return val2;
                }));
        return resultMap;
    }
}
