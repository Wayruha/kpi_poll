package edu.kpi.service;

import edu.kpi.model.dto.AcademicDto;
import edu.kpi.model.entity.Academic;
import edu.kpi.model.entity.AveragePoint;
import edu.kpi.repository.AcademicRep;
import edu.kpi.repository.AveragePointRep;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AcademicService {

    private AcademicRep academicRep;
    private AveragePointRep averagePointRep;

    public AcademicService(AcademicRep academicRep, AveragePointRep averagePointRep) {
        this.academicRep = academicRep;
        this.averagePointRep = averagePointRep;
    }

    public void updateStatistic(Academic academic, double mark, boolean insertOperation) {
        AveragePoint averagePoint = Optional.ofNullable(averagePointRep.findOne(academic.getAcademicId())).orElse(new AveragePoint());
        averagePoint.setAveragePointId(academic.getAcademicId());
        double prevVal = averagePoint.getAverage();
        int count = averagePoint.getVotesCount();

        Double newAverage = calculateNewAverage(prevVal, count, mark, insertOperation);
        averagePoint.setVotesCount(insertOperation ? ++count : --count);
        averagePoint.setAverage(newAverage);
        averagePointRep.save(averagePoint);
    }

    private double calculateNewAverage(double oldVal, int count, double mark, boolean insert) {
        double diff = 0;
        if (count <= 0 && insert) {
            return mark;
        } else if (count == 1 && !insert) {
            return 0;
        }
        count = insert ? count + 1 : count - 1;

        diff = (oldVal - mark) / count;

        return insert ? oldVal - diff : oldVal + diff;
    }

    public List<AcademicDto> getAllBasicAcademicDetails(Integer page, Integer count, String query) {
        if (count == null || count == 0) {
            count = Integer.MAX_VALUE;
        }
        PageRequest pageable = new PageRequest(page, count);
        if (query == null || query.length() < 1) {
            return academicRep.findAcademicsDetails(pageable);
        }
        query = "%" + query.toLowerCase() + "%";
        return academicRep.findAcademicsDetails(query, pageable);

    }

    public Integer getAcademicCount(String query) {
        if (query != null && query.trim().length() >= 1) {
            query = "%" + query.toLowerCase() + "%";
        } else {
            query = null;
        }
        return academicRep.findAcademicsDetailsCountOnly(query);
    }

    public List<AcademicDto> getAllBasicAcademicDetails(List<Long> ids) {
        return academicRep.findAcademicsDetails(ids, new PageRequest(0, Integer.MAX_VALUE));
    }


    public List<Academic> getAcademicsBySourceId(List<String> sourceIds) {
        return academicRep.findAcademicBySourceId(sourceIds);
    }


    public AveragePoint getAverageForAcademic(Academic academic) {
        return averagePointRep.findOne(academic.getAcademicId());
    }

    public AcademicDto getAcademicDto(Long id) {
        return academicRep.findAcademicDetails(id);
    }

    public Academic getById(Long id) {
        return academicRep.findOne(id);
    }
}
