package edu.kpi.service;

import edu.kpi.exception.LoadScheduleException;
import edu.kpi.model.entity.Academic;

import java.util.List;

public interface AcademicScheduleProvider {

    List<Academic> getAcademicsForGroup(String groupName) throws LoadScheduleException;
}
