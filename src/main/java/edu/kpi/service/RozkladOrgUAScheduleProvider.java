package edu.kpi.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.kpi.exception.LoadScheduleException;
import edu.kpi.external.AcademicResponse;
import edu.kpi.external.HttpClient;
import edu.kpi.external.HttpResponse;
import edu.kpi.model.entity.Academic;
import edu.kpi.repository.AcademicRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RozkladOrgUAScheduleProvider implements AcademicScheduleProvider {

    @Autowired
    private HttpClient httpClient;

    @Autowired
    private AcademicRep academicRep;

    private final static String TEACHERS_FOR_GROUP_PLACEHOLDER = "https://api.rozklad.org.ua/v2/groups/%s/teachers";

    @Override
    public List<Academic> getAcademicsForGroup(String groupName) throws LoadScheduleException {
        Set<Academic> resultSet = new HashSet<>();
        String url = buildUrl(groupName);
        try {
            String responseJSON = httpClient.doGetRequest(url);
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            HttpResponse response = objectMapper.readValue(responseJSON, HttpResponse.class);
            List<String> sourceIds = response.getData().stream().map(AcademicResponse::getTeacher_id).collect(Collectors.toList());
            return academicRep.findAcademicBySourceId(sourceIds);
        } catch (IOException e) {
            e.printStackTrace();
            throw new LoadScheduleException("Can't load schedule", e, groupName);
        }
    }


    private String buildUrl(String groupName) {
        return String.format(TEACHERS_FOR_GROUP_PLACEHOLDER, groupName);
    }
}