package edu.kpi.service;

import edu.kpi.model.Student;
import edu.kpi.model.dto.AcademicCriterionSummary;
import edu.kpi.model.entity.Academic;
import edu.kpi.model.entity.Criterion;
import edu.kpi.model.entity.DetailCode;
import edu.kpi.model.entity.Vote;

import java.util.List;
import java.util.Map;


public interface VoteService {

    Vote vote(Student stud, Academic academic, Criterion criteria, DetailCode value);

    Vote vote(Student stud, Academic academic, Long criterionId, Long dtlCodeId);

    List<AcademicCriterionSummary> getAcademicResults(long academicId);

    AcademicCriterionSummary findAverageValueForAcademicAndCriterion(Academic academic, Criterion criterion);

    Map<Criterion, DetailCode> getVotesByStudentAndCriteria(Long academicId, Student student, List<Criterion> criteria);
}
