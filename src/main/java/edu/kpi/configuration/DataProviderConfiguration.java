package edu.kpi.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"edu.kpi.repository", "edu.kpi.service"})
@EntityScan("edu.kpi.model.entity")
public class DataProviderConfiguration {

}
