package edu.kpi.configuration;

import edu.kpi.model.Student;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AuthentificationConfig {

    @Qualifier("loggedIn")
    @Bean
    //@SessionScope
    public Student getLoggedInStudent() {
        Student stud = new Student();
        stud.setFaculty("ФІОТ");
        stud.setGroup("ІТ-42");
        stud.setKpiId("user11");
        return stud;
    }
}
