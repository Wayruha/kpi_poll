package edu.kpi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KpiPollApplication {

    public static void main(String[] args) {
        SpringApplication.run(KpiPollApplication.class, args);
    }
}
