package edu.kpi.controller;

import edu.kpi.model.dto.AcademicCriterionSummary;
import edu.kpi.model.dto.AcademicDto;
import edu.kpi.service.AcademicService;
import edu.kpi.service.VoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/academic")
public class AcademicController {

    @Autowired
    private VoteService voteService;

    @Autowired
    private AcademicService academicService;

    @GetMapping("{academicId}/result")
    @ResponseBody
    public List<AcademicCriterionSummary> getAcademicPollResult(@PathVariable Long academicId) {
        if (academicId == null || academicId == 0) {
            throw new IllegalArgumentException("Academic was not specified");
        }

        List<AcademicCriterionSummary> summaryList = voteService.getAcademicResults(academicId);

        return summaryList;
    }

    @GetMapping("{academicId}")
    @ResponseBody
    public AcademicDto getAcademicRecord(@PathVariable Long academicId) {
        if (academicId == null || academicId == 0) {
            throw new IllegalArgumentException("Academic was not specified");
        }

        return academicService.getAcademicDto(academicId);
    }

}
