package edu.kpi.controller;

import edu.kpi.model.Student;
import edu.kpi.model.dto.AcademicDto;
import edu.kpi.model.entity.Academic;
import edu.kpi.model.entity.Criterion;
import edu.kpi.model.entity.DetailCode;
import edu.kpi.repository.DetailCodeRep;
import edu.kpi.service.AcademicService;
import edu.kpi.service.CriterionService;
import edu.kpi.service.VoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Controller
@RequestMapping("/poll")
public class VoteController {

    @Autowired
    private Student principal;
    private AcademicService academicService;
    private CriterionService criterionService;
    private DetailCodeRep detailCodeRep;
    private VoteService voteService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public VoteController(AcademicService academicService, CriterionService criterionService, DetailCodeRep detailCodeRep, VoteService voteService) {
        this.academicService = academicService;
        this.criterionService = criterionService;
        this.detailCodeRep = detailCodeRep;
        this.voteService = voteService;
    }

    @GetMapping(path = "/{academicId}")
    public ModelAndView votePage(@PathVariable Long academicId) {
        ModelAndView mav = new ModelAndView("voting");
        try {
            AcademicDto academicDto = academicService.getAcademicDto(academicId);
            if (academicDto == null) throw new IllegalArgumentException("Can't find academic");
            List<Criterion> academicCriteria = criterionService.getAcademicCriteria();
            Map<Criterion, List<DetailCode>> criteriaCodesMap = academicCriteria.stream().collect(Collectors.toMap(cr -> cr, detailCodeRep::getByCriterion));
            Map<Criterion, DetailCode> votesByStudent = voteService.getVotesByStudentAndCriteria(academicId, principal, academicCriteria);
            mav.addObject("academic", academicDto);
            mav.addObject("criteria", academicCriteria);
            mav.addObject("criteriaCodesMap", criteriaCodesMap);
            if (votesByStudent != null && !votesByStudent.isEmpty()) {
                mav.addObject("votesMap", votesByStudent);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw ex;
            // mav.addObject("error", ex.getMessage());
        }
        return mav;
    }

    @PostMapping(path = "/{academicId}/vote", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseBody
    public HttpStatus vote(@PathVariable Long academicId, @RequestParam Map<String, String> votesMap) {
        try {
            Academic academic = academicService.getById(academicId);
            for (Map.Entry<String, String> entry : votesMap.entrySet()) {
                Long criterionId = Long.valueOf(entry.getKey());
                Long detaildCodeId = Long.valueOf(entry.getValue());

                voteService.vote(principal, academic, criterionId, detaildCodeId);
            }

        } catch (NumberFormatException ex) {
            logger.error(ex.getMessage(), ex);
            throw new IllegalArgumentException(ex);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw ex;
        }
        return HttpStatus.OK;
    }

    @ExceptionHandler
    public String exceptionHandler(Exception ex) {
        return "error";
    }
}
