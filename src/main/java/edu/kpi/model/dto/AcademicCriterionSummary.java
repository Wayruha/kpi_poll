package edu.kpi.model.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class AcademicCriterionSummary {
    private final Long academicId;
    private final Long criterionId;
    private final String criterionDesc;
    private double averageValue;
    private int totalVoteCount;

    public void setAverageValue(Double val) {
        this.averageValue = Math.floor(val * 100) / 100;
    }

    public Double getAverageValue() {
        return Math.floor(this.averageValue * 100) / 100;
    }
}
