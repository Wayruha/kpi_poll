package edu.kpi.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.text.DecimalFormat;
import java.util.Optional;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AcademicDto {
    private final static DecimalFormat format = new DecimalFormat("#.##");

    private Long academicId;
    private String fullName;
    private String position;
    private String label;
    private Double averagePoint;
    private Integer totalVoteCount;

    public void setAveragePoint(Double val) {
        this.averagePoint = Math.floor(val * 100) / 100;
    }

    public Double getAveragePoint() {
        return Math.floor(Optional.ofNullable(this.averagePoint).orElse(0d) * 100) / 100;
    }
}
