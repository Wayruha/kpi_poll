package edu.kpi.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "\"VOTE\"")
public class Vote extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "VOTE_ID")
    private Long voteId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACADEMIC_ID")
    private Academic academic;

    @Column(name = "STUDENT_ID")
    private String studentId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CRITERION_ID")
    private Criterion criterion;

    @OneToOne
    @JoinColumn(name = "VALUE")
    private DetailCode value;

    public Long getVoteId() {
        return voteId;
    }

    public void setVoteId(Long voteId) {
        this.voteId = voteId;
    }

    public Academic getAcademic() {
        return academic;
    }

    public void setAcademic(Academic academic) {
        this.academic = academic;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public Criterion getCriterion() {
        return criterion;
    }

    public void setCriterion(Criterion criterion) {
        this.criterion = criterion;
    }

    public DetailCode getValue() {
        return value;
    }

    public void setValue(DetailCode value) {
        this.value = value;
    }
}
