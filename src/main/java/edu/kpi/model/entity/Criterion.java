package edu.kpi.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;


@Entity
@Table(name = "CRITERION")
public class Criterion extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CRITERION_ID")
    private Long criterionId;

    @Column(name = "CODE")
    private String code;

    @Column(name = "DESC")
    private String description;

    @ManyToOne
    @JoinColumn(name = "POLL_TYPE_ID")
    private PollType pollType;

    @Column(name = "GROUP_RESTRICTION")
    private String groupRestriction;

    @Column(name = "FACULTY_RESTRICTION")
    private String facultyRestriction;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "CRITERION_DETAIL_CODE",
            joinColumns = @JoinColumn(name = "CRITERION_ID"),
            inverseJoinColumns = @JoinColumn(name = "DETAIL_CODE_ID")
    )
    private Set<DetailCode> detailCodes;

    public Set<DetailCode> getDetailCodes() {
        return detailCodes;
    }

    public void setDetailCodes(Set<DetailCode> detailCodes) {
        this.detailCodes = detailCodes;
    }

    public Long getCriterionId() {
        return criterionId;
    }

    public void setCriterionId(Long criterionId) {
        this.criterionId = criterionId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PollType getPollType() {
        return pollType;
    }

    public void setPollType(PollType pollType) {
        this.pollType = pollType;
    }

    public String getGroupRestriction() {
        return groupRestriction;
    }

    public void setGroupRestriction(String groupRestriction) {
        this.groupRestriction = groupRestriction;
    }

    public String getFacultyRestriction() {
        return facultyRestriction;
    }

    public void setFacultyRestriction(String facultyRestriction) {
        this.facultyRestriction = facultyRestriction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Criterion criterion = (Criterion) o;

        return criterionId.equals(criterion.criterionId);
    }

    @Override
    public int hashCode() {
        return criterionId.hashCode();
    }
}
