package edu.kpi.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(name = "AVERAGE_POINT")
public class AveragePoint extends BaseEntity {
    @Id
    @Column(name = "ACADEMIC_ID")
    private Long averagePointId;

    @JsonBackReference
    @OneToOne(mappedBy = "averagePoint")
    private Academic academic;

    @Column(name = "AVERAGE")
    private double average;

    @Column(name = "VOTES_COUNT")
    private int votesCount;

    public Long getAveragePointId() {
        return averagePointId;
    }

    public void setAveragePointId(Long averagePointId) {
        this.averagePointId = averagePointId;
    }

    public Academic getAcademic() {
        return academic;
    }

    public void setAcademic(Academic academic) {
        this.academic = academic;
    }

    public Double getAverage() {
        return average;
    }

    public void setAverage(Double average) {
        this.average = average;
    }

    public Integer getVotesCount() {
        return votesCount;
    }

    public void setVotesCount(Integer votesCount) {
        this.votesCount = votesCount;
    }
}
