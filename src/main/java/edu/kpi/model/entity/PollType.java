package edu.kpi.model.entity;


import javax.persistence.*;

@Entity
@Table(name = "POLL_TYPE")
public class PollType extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "POLL_TYPE_ID")
    private Long pollTypeId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESC")
    private String description;

    @Column(name = "REF_PREFIX")
    private String referencePrefix;

    public Long getPollTypeId() {
        return pollTypeId;
    }

    public void setPollTypeId(Long pollTypeId) {
        this.pollTypeId = pollTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReferencePrefix() {
        return referencePrefix;
    }

    public void setReferencePrefix(String referencePrefix) {
        this.referencePrefix = referencePrefix;
    }
}

