package edu.kpi.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "DETAIL_CODE")
public class DetailCode extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DETAIL_CODE_ID")
    private Long detailCodeId;

    @Column(name = "CODE")
    private String code;

    @Column(name = "VALUE")
    private String value;

    @JsonIgnore
    @ManyToMany(mappedBy = "detailCodes")
    private Set<Criterion> criterion;

    public Long getDetailCodeId() {
        return detailCodeId;
    }

    public void setDetailCodeId(Long detailCodeId) {
        this.detailCodeId = detailCodeId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Set<Criterion> getCriterion() {
        return criterion;
    }

    public void setCriterion(Set<Criterion> criterion) {
        this.criterion = criterion;
    }
}
