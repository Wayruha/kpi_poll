<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>test</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/normalize.css"/>">
    <link rel="stylesheet" href="<c:url value="/resources/css/first.css"/>">
    <link rel="stylesheet" href="<c:url value="/resources/libs/BT/bootstrap.min.css"/>">
    <link href="http://allfont.ru/allfont.css?fonts=montserrat" rel="stylesheet" type="text/css"/>
    <link href="http://allfont.ru/allfont.css?fonts=montserrat-hairline" rel="stylesheet" type="text/css"/>
</head>
<body>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="logo"><img src='<c:url value="/resources/img/14669645137640.png"/>' alt=""></div>
                <h1>Ласкаво просимо!</h1>
                <p>Щоб взяти участь в опитуванні, будь ласка, авторизуйтесь</p>
                <div class="btn" data-type="pos_aware">
                    <a href="<c:url value="/login"/>">Увійти за допомогою KPI ID</a>
                </div>
            </div>
        </div>
    </div>

</div>


<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/commom.js"></script>
</body>
</html>