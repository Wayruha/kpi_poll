<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>test3</title>
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/label.css">

</head>
<body>
<div class="navbar-component">
    <div class="navbar area">
        <a href="#" class="brand">Logo</a>
        <nav role="navigation" id="navigation" class="list">


            <a href="pollMain.jsp" class="item -link active">ОПИТУВАННЯ</a>
            <a href="academic.jsp" class="item -link">РЕЙТИНГ</a>
            <a href="example3.jsp" class="item -link">ПРЕДМЕТИ</a>
        </nav>
    </div>
</div>


<div class="container">
    <div class="control-group">
        <h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h1>
        <input type="radio" name="gender" value="male"> Female<br>
        <input type="radio" name="gender" value="female"> Female<br>
        <input type="radio" name="gender" value="other"> Female

        <h1>Lorem ipsum dolor.</h1>
        <input type="radio" name="gender" value="male"> Female<br>
        <input type="radio" name="gender" value="female"> Female<br>
        <input type="radio" name="gender" value="other"> Female

        <h1>Checkboxes</h1>
        <input type="radio" name="gender" value="male"> Female<br>
        <input type="radio" name="gender" value="female"> Female<br>
        <input type="radio" name="gender" value="other"> Female

        <h1>Checkboxes</h1>
        <input type="radio" name="gender" value="male"> Female<br>
        <input type="radio" name="gender" value="female"> Female<br>
        <input type="radio" name="gender" value="other"> Female
        <button type="submit" data-type="pos_aware">Submit</button>
    </div>
</div>

</body>
</html>