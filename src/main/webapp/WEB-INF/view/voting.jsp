<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>${academic.label}</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/stars.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/grid.css"/>">
    <script type="text/javascript" src="<c:url value="/resources/js/jquery.min.js"/>"></script>
</head>

<body>
<div id="wrapper">
    <div class="navbar-component">
        <%@include  file="header.jsp"%>
    </div>

    <div class="grid grid-pad">

        <div class="timetable">
            <div class="timetable-head-light">
                <div class="text-center">
                    <h2 style="display: inline-block">Оцінка викладача</h2>
                </div>
            </div>
            <c:forEach items="${criteria}" var="criterion">
                <div class="timetable-cell">
                    <div class="grid grid-pad">
                        <div class="col-3-12">
                            <div class="content">
                                <h3 class="rating-title">${criterion.description}</h3>
                            </div>
                        </div>
                        <div class="col-9-12">
                                <span class="star-rating" id="rating-${criterion.criterionId}">
                                    <c:forEach items="${criteriaCodesMap.get(criterion)}" var="dtlCode">
                                        <input type="radio" name="${criterion.criterionId}"
                                               value="${dtlCode.detailCodeId}">
                                        <i></i>
                                    </c:forEach>
                                </span>
                        </div>
                    </div>
                </div>
            </c:forEach>
            <div class="row">
                <h3 class="col-4-12">Середня оцінка:${academic.averagePoint}</h3>
                <div class="col-4-12"><input type="button" class="submitButton" id="submitBtn"
                                             onclick="submitVote()" value="Голосувати"/>
                </div>
                <h3 class="col-4-12">Всього голосів:${academic.totalVoteCount}</h3>
            </div>
            <div class="col-4-12"  style="visibility: hidden;"><a href="/">Повернутися до списку</a></div>
        </div>
        <div id="info" style="font:red"></div>
        <div class="col-4-12"><a href="/">Повернутися до списку</a></div>
    </div>

</div>
<script type="text/javascript">
    <c:forEach items="${votesMap}" var="vote">
    document.getElementById("rating-${vote.key.criterionId}").getElementsByTagName("input")[${vote.value.detailCodeId-1}].checked = true;
    </c:forEach>

    function submitVote() {
        var map = {};
        $('.star-rating input:checked').each(function (index, vote) {
            map[$(vote).attr('name')] = $(vote).attr('value');
        });
        console.log(map);
        $.ajax({
            type: 'POST',
            url: '${academic.academicId}/vote',
            data: map,
            error: function (err) {
                console.log(err);
                $('#info').html('<p>Сталася помилка. Будь ласка, спробуйте пізніше</p>');
            },
            success: function (data) {
                window.location = '/';
            }
        });
    }

</script>

</body>
</html>  