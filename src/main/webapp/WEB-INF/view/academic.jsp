<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Всі викладачі</title>
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/styles.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/test.css"/>"/>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery.min.js"/>"></script>
</head>
<body>
<div id="wrapper">
    <div class="navbar-component">
        <%@include file="header.jsp" %>
    </div>

    <div id="paging_container11" class="container">
        <div class="page_navigation"></div>

        <input class="search" id="queryInput" name="nomer2" value="" size="20" type="text"
               style="background: url('<c:url value="/resources/img/find.png"/>') no-repeat scroll 5% 50% rgb(0, 0, 0);"
               onkeypress="return onKeyPress(event)">
        <ul class="content">
            <c:if test="${academics.size() == 0}">
                <div style="font:red;">Не вдалося завантажити список викладачів.</div>
            </c:if>
            <c:forEach var="academic" items="${academics}">
                <div class="timetable-cell clearfix"><span class='left'>
                            <a hr ef='#'><u>${academic.fullName}</u></a>
							<a href='#'><span style='color: indianred'>${academic.averagePoint}</span></a>
                    </span><span class='right'><a class="open_window" onclick="showDetailPopup(${academic.academicId})">Детальніше</a></span>
                </div>
            </c:forEach>

            <li>
        </ul>
        <center>Сторінка ${page+1} з ${pageCount+1} </center>
        <s:eval expression="T(java.lang.Math).max(0,page-1)" var="previousPage"/>
        <c:if test="${page != 0}">
            <a href="/academic?page=${previousPage}&query=${query}" class="pagination"><<</a>
        </c:if>
        <c:if test="${page < pageCount - 1}">
            <a href="/academic?page=${page+1}&query=${query}" class="pagination">>></a>
        </c:if>
    </div>

</div>


<div class="overlay" title="">
    <div id="form">

        <span class="heading">Рейтинг викладача</span>
        <p id="headerError">Сталася помилка. Будь ласка, спробуйте пізніше</p>
        <div id="resultHeader">
            <span id="fa-star-1" class="fa fa-star"></span>
            <span id="fa-star-2" class="fa fa-star"></span>
            <span id="fa-star-3" class="fa fa-star"></span>
            <span id="fa-star-4" class="fa fa-star"></span>
            <span id="fa-star-5" class="fa fa-star"></span>

            <div class="col-6-12"><p>Середня оцінка: <span id="averageMark">3.0</span></p></div>
            <div class="col-6-12" style="text-align: right"><p>Всього голосів: <span id="totalVoteCount">254</span></p>
            </div>
        </div>
        <hr style="border:3px solid #f1f1f1">

        <div class="row" id="popupBody">
            <%--content--%>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('li:odd, .content > *:odd').css('background-color', '#c7c7c7');
        $('.popup .close_window, .overlay').click(function () {
            $('.popup, .overlay').css({'opacity': '0', 'visibility': 'hidden'});
            $('#popupBody').html('');
        });

        $('#queryInput').val('${query}');
    });


    function showDetailPopup(academicId) {
        loadAcademicHeader(academicId);
        $.ajax({
            type: 'get',
            url: 'academic/' + academicId + '/result',
            error: onDetailError,
            success: function (data) {
                if (data.length == 0) {
                    $('#popupBody').append('<p>Результатів немає</p>');
                } else {
                    data.forEach(function (row, index) {
                        var average = parseInt(row['averageValue']);
                        $('#popupBody').append('<div class="side"><div>' + row['criterionDesc'] + '</div></div>');
                        $('#popupBody').append('<div class="middle"><div class="bar-container"><div class="bar-' + average + '"></div></div></div>');
                        $('#popupBody').append('<div class="side right"><div>' + row['totalVoteCount'] + '</div></div>');
                    });
                }
            }
        });

        $('.popup, .overlay').css({'opacity': '1', 'visibility': 'visible'});
        return false;
    }

    function loadAcademicHeader(academicId) {
        $.ajax({
            type: 'get',
            url: 'academic/' + academicId,
            error: onHeaderError,
            success: function (data) {
                $('#headerError').hide();
                $('#resultHeader').show();
                if (data['averagePoint'] == undefined || data['totalVoteCount'] == undefined) {
                    $('#resultHeader').hide();
                } else {
                    $('#averageMark').html(data['averagePoint']);
                    $('#totalVoteCount').html(data['totalVoteCount']);
                    setStars(parseInt(data['averagePoint']));
                }
            }
        });
    }

    function onHeaderError(err) {
        console.log("Load Header Error");
        console.log(err);
        $('#headerError').show();
        $('#resultHeader').hide();
    }

    function onDetailError(err) {
        console.log("Load Details Error");
        console.log(err);
        $('#popupBody').append('<p>Сталася помилка. Будь ласка, спробуйте пізніше</p>');
    }

    function onKeyPress(event) {
        if (event.keyCode == 13) {
            var query = $('#queryInput').val();
            window.location = '/academic?page=0&query=' + query;
            return false;
        }
        return true;
    }

    function setStars(starCount) {
        //clear stars
        for (i = 1; i <= 5; i++) {
            $('#fa-star-' + i).removeClass('checked');
        }
        for (i = 1; i <= starCount; i++) {
            $('#fa-star-' + i).addClass('checked');
        }
    }

</script>

</body>
</html>