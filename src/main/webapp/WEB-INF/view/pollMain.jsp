<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Викладачі</title>
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/styles.css"/>"/>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery.min.js"/>"></script>
    <style>body {
        font-family: Verdana, Arial, sans-serif;
    }</style>

</head>
<body>

<div id="wrapper">

    <div class="navbar-component">
        <%@include  file="header.jsp"%>
    </div>

    <div id="paging_container11" class="container">
        <h2>Ваші відверті відповіді дозволять отримати важливу інформацію для підвищення ефективності
            навчально-виховного процесу. Ваші відповіді конфіденційні й будуть використані лише в узагальненому
            вигляді.</h2>
        <div class="page_navigation"></div>


        <ul class="content">
            <c:if test="${academics.size() == 0}">
                <div style="font:red">Не вдалося завантажити викладачів. Можливо ресурс rozklad.org.ua не доступний
                </div>
            </c:if>
            <c:forEach var="academic" items="${academics}">
                <div class="timetable-cell clearfix">
                    <span class='left'>
                        <u><span> ${academic.fullName}</span></u>
                            <span style='color:#00796b'>${academic.averagePoint}</span>
                            </span><span class='right'><a href='/poll/${academic.academicId}'>Голосувати</a>
                </span>
                </div>
            </c:forEach>

            <li>
        </ul>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('li:odd, .content > *:odd').css('background-color', '#c7c7c7');
    });
</script>
</body>
</html>