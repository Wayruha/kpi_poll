<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Сталася помилка!</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/normalize.css"/>">
    <link rel="stylesheet" href="<c:url value="/resources/css/first.css"/>">
    <link rel="stylesheet" href="<c:url value="/resources/libs/BT/bootstrap.min.css"/>">
    <link href="http://allfont.ru/allfont.css?fonts=montserrat" rel="stylesheet" type="text/css"/>
    <link href="http://allfont.ru/allfont.css?fonts=montserrat-hairline" rel="stylesheet" type="text/css"/>
</head>
<body>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="logo"><img src='<c:url value="/resources/img/14669645137640.png"/>' alt=""></div>
                <h1>Помилка!</h1>
                <h3>На жаль, на сервері виникла помилка. Будь ласка, спробуйте пізніше</h3>
            </div>
        </div>
    </div>

</div>


<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/commom.js"></script>
</body>
</html>