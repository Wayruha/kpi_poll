package edu.kpi.service;


import edu.kpi.configuration.AuthentificationConfig;
import edu.kpi.configuration.DataProviderConfiguration;
import edu.kpi.external.HttpClient;
import edu.kpi.model.dto.AcademicDto;
import edu.kpi.model.entity.Academic;
import edu.kpi.model.entity.AveragePoint;
import edu.kpi.repository.AcademicRep;
import edu.kpi.repository.AveragePointRep;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {DataProviderConfiguration.class, AuthentificationConfig.class, HttpClient.class})
@ComponentScan(basePackages = {"edu.kpi.repository", "edu.kpi.service", "edu.kpi.external"})
@DataJpaTest
public class AcademicServiceTest {

    @Autowired
    private AcademicRep academicRep;
    @Autowired
    private AveragePointRep averagePointRep;
    @Autowired
    private AcademicService academicService;

    private Academic KORNAGA;
    private AveragePoint KORNAGA_POINTS;

    @Before
    public void initData() {
        KORNAGA = new Academic();
        KORNAGA.setAcademicId(1l);
        KORNAGA.setSourceId("1s2");
        KORNAGA.setFullName("Корнага Ярослав Ігорович");
        KORNAGA.setPosition("декан ФІОТ Корнага Ярослав Ігорович");
        KORNAGA.setLabel("дк. Корнага Я.І.");

        KORNAGA_POINTS = new AveragePoint();
        KORNAGA_POINTS.setAveragePointId(1l);
        KORNAGA_POINTS.setAverage(3.5);
        KORNAGA_POINTS.setVotesCount(4);
        KORNAGA_POINTS.setAcademic(KORNAGA);

    }

    @Test
    public void getAcademicTest() {
        Academic academic = academicRep.findOne(1l);
        Assert.assertEquals(academic.getSourceId(), "1s2");
    }

    @Test
    public void getAllAcademicsTest() {
        Set<Academic> academics = academicRep.findAll();
        Assert.assertEquals(academics.size(), 4);
    }

    @Test
    public void getAllAveragePointsTest() {
        Set<AveragePoint> averagePoints = averagePointRep.findAll();
        Assert.assertEquals(averagePoints.size(), 2);
    }

    @Test
    public void createNewAcademicTest() {
        int countBefore = academicRep.findAll().size();
        Academic academic = new Academic();
        academic.setPosition("Дячук Оман");
        academic.setLabel("Romashka");
        academic.setFullName("DRV- full name");
        academic = academicRep.save(academic);
        int countAfter = academicRep.findAll().size();
        Assert.assertEquals(1, countAfter - countBefore);
        Assert.assertEquals(5, (long) academic.getAcademicId());
    }


    @Test
    public void createNewAcademicAverageTest() {
        int countBefore = averagePointRep.findAll().size();
        Academic academic = academicRep.findOne(3l);
        AveragePoint averagePoint = new AveragePoint();
        averagePoint.setAveragePointId(academic.getAcademicId());
        averagePoint.setVotesCount(1);
        averagePoint.setAverage(5d);
        averagePoint.setAcademic(academic);
        averagePoint = averagePointRep.save(averagePoint);
        int countAfter = averagePointRep.findAll().size();
        Assert.assertEquals(1, countAfter - countBefore);
        Assert.assertEquals(3, (long) averagePoint.getAveragePointId());
    }

    @Test
    public void getAcademicAverageTest() {
        AveragePoint averagePoint = academicService.getAverageForAcademic(KORNAGA);
        Assert.assertEquals((double) averagePoint.getAverage(), 3.5, 1);
    }

    @Test
    public void getSingleAcademicDto() {
        List<AcademicDto> dtos = academicRep.findAcademicsDetails(Arrays.asList(KORNAGA.getAcademicId()), null);
        AcademicDto kornagaDto = dtos.iterator().next();
        Assert.assertEquals(KORNAGA.getFullName(), kornagaDto.getFullName());
        Assert.assertEquals(KORNAGA.getPosition(), kornagaDto.getPosition());
        Assert.assertEquals(KORNAGA.getLabel(), kornagaDto.getLabel());
        Assert.assertEquals(KORNAGA_POINTS.getAverage(), kornagaDto.getAveragePoint());
    }

    @Test
    public void getAcademicsBySourceId() {
        List<String> sourceIds = Arrays.asList("1s2");
        List<Academic> academics = academicService.getAcademicsBySourceId(sourceIds);
        Assert.assertEquals(1, academics.size());
        Academic academic = academics.iterator().next();
        Assert.assertEquals(1, (long) academic.getAcademicId());
    }
}
