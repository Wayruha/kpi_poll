package edu.kpi.service;

import edu.kpi.configuration.AuthentificationConfig;
import edu.kpi.configuration.DataProviderConfiguration;
import edu.kpi.external.HttpClient;
import edu.kpi.model.Student;
import edu.kpi.model.dto.AcademicCriterionSummary;
import edu.kpi.model.entity.*;
import edu.kpi.repository.VoteRep;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {DataProviderConfiguration.class, AuthentificationConfig.class, HttpClient.class})
@ComponentScan(basePackages = {"edu.kpi.repository", "edu.kpi.service", "edu.kpi.external"})
@DataJpaTest
public class VoteServiceImplTest {

    @Autowired
    private VoteService voteService;

    @Autowired
    private AcademicService academicService;

    @Autowired
    private VoteRep voteRep;

    @Qualifier("loggedIn")
    @Autowired
    private Student student;

    private Academic kornaga;
    private AveragePoint KORNAGA_POINTS;
    private Criterion communicativeness;
    private static final double DELTA = 1e-15;
    private static final double EXPECTED_AVERAGE = 3.5;
    private static final int EXPECTED_VOTES_COUNT = 4;

    @Before
    public void initData() {
        kornaga = new Academic();
        kornaga.setAcademicId(1l);
        kornaga.setSourceId("1s2");
        kornaga.setFullName("Корнага Ярослав Ігорович");
        kornaga.setPosition("декан ФІОТ Корнага Ярослав Ігорович");
        kornaga.setLabel("дк. Корнага Я.І.");

        KORNAGA_POINTS = new AveragePoint();
        KORNAGA_POINTS.setAveragePointId(1l);
        KORNAGA_POINTS.setAverage(3.5);
        KORNAGA_POINTS.setVotesCount(4);
        KORNAGA_POINTS.setAcademic(kornaga);

        communicativeness = new Criterion();
        communicativeness.setCriterionId(1l);
        communicativeness.setCode("communicative");
        communicativeness.setDescription("How communicative academic is?");

    }

    @Test
    public void findSingleAverageForAcademicAndCriterion() {
        AcademicCriterionSummary results = voteService.findAverageValueForAcademicAndCriterion(kornaga, communicativeness);
        Assert.assertEquals(communicativeness.getDescription(), results.getCriterionDesc());

        Assert.assertEquals(EXPECTED_AVERAGE, results.getAverageValue(), DELTA);
        Assert.assertEquals(EXPECTED_VOTES_COUNT, results.getTotalVoteCount());
    }

    @Test
    public void voteTest() throws Exception {
        final int countBefore = voteRep.findAll().size();
        DetailCode detailCode = new DetailCode();
        detailCode.setDetailCodeId(1l);

        Vote vote = voteService.vote(student, kornaga, communicativeness, detailCode);

        final int countAfter = voteRep.findAll().size();
        Assert.assertNotNull(vote.getVoteId());
        Assert.assertNotEquals(0, (long) vote.getVoteId());
        Assert.assertEquals(1, countAfter - countBefore);

        AveragePoint averagePoint = academicService.getAverageForAcademic(kornaga);
        Assert.assertEquals(3.5, averagePoint.getAverage(), DELTA);
        Assert.assertEquals(5, (int) averagePoint.getVotesCount());
    }

    @Test
    public void getAllVotes() throws Exception {
        List<Vote> result = voteRep.findAll();//voteService.getAcademicResults(academic);
        Assert.assertEquals(result.size(), 8);
    }

    @Test
    public void getVotesByStudentAndCriteriaTest() {
        long academicId = 2l;
        Criterion crit1 = new Criterion();
        crit1.setCriterionId(1l);
        Criterion crit2 = new Criterion();
        crit2.setCriterionId(2l);
        List<Criterion> criteria = Arrays.asList(crit1, crit2);

        Map resultMap = voteService.getVotesByStudentAndCriteria(academicId, student, criteria);
        Assert.assertEquals(2, resultMap.size());
    }

}