package edu.kpi.service;

import edu.kpi.configuration.AuthentificationConfig;
import edu.kpi.configuration.DataProviderConfiguration;
import edu.kpi.exception.LoadScheduleException;
import edu.kpi.external.HttpClient;
import edu.kpi.model.entity.Academic;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {DataProviderConfiguration.class, AuthentificationConfig.class, HttpClient.class})
@ComponentScan(basePackages = {"edu.kpi.repository", "edu.kpi.service", "edu.kpi.external"})
@DataJpaTest
public class RozkladOrgUAScheduleServiceTest {

    @Autowired
    private RozkladOrgUAScheduleProvider scheduleProvider;

    private static final String GROUP_NAME = "іт-42";

    @Test
    public void getAcademicsForGroup() throws LoadScheduleException {
        List<Academic> academics = scheduleProvider.getAcademicsForGroup(GROUP_NAME);
        Assert.assertEquals(6, academics.size());
    }
}
