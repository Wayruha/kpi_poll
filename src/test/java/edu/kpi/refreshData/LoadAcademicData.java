package edu.kpi.refreshData;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.kpi.configuration.AuthentificationConfig;
import edu.kpi.configuration.DataProviderConfiguration;
import edu.kpi.external.AcademicResponse;
import edu.kpi.external.HttpClient;
import edu.kpi.external.HttpResponse;
import edu.kpi.external.RozkladOrgMeta;
import edu.kpi.model.entity.Academic;
import edu.kpi.repository.AcademicRep;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@ActiveProfiles("dev")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {DataProviderConfiguration.class, AuthentificationConfig.class, HttpClient.class})
@ComponentScan(basePackages = {"edu.kpi.repository", "edu.kpi.service", "edu.kpi.external"})
@PropertySource("classpath:/external-api.properties")
@DataJpaTest
public class LoadAcademicData {

    @Autowired
    private HttpClient httpClient;

    @Autowired
    private AcademicRep academicRep;

    @Value("${scheduleProvider.academics.url}") //http://api.rozklad.org.ua/v2/teachers
    private String academicsUrl;

    private String ACADEMIC_TABLE = "ACADEMIC";
    private String ACADEMIC_COLUMNS = "SOURCE_ACADEMIC_ID,FULL_NAME,POSITION,LABEL";

    //TODO need to handle more complex cases. Need to find method to identify academic
    @Test
    public void fetchData() throws IOException {

        Set<AcademicResponse> newAcademicsResponse = new HashSet<>();
        RozkladOrgMeta meta;
        int offset = 0;
        final int defaultLimit = 100;
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        do {
            String responseJSON = httpClient.doGetRequest(academicsUrl, getFilterParam(offset, defaultLimit));

            HttpResponse response = objectMapper.readValue(responseJSON, HttpResponse.class);
            newAcademicsResponse.addAll(response.getData());

            meta = response.getMeta();
            offset += meta.getLimit();
        } while (meta.getOffset() + meta.getLimit() < meta.getTotal_count());

        Set<String> currentSourceIds = loadCurrentAcademics().stream().map(Academic::getSourceId).collect(Collectors.toSet());

        newAcademicsResponse.stream()
                .filter(resp -> !currentSourceIds.contains(resp.getTeacher_id())).collect(Collectors.toList());

        List<String> generatedSql = newAcademicsResponse.stream().map(this::generateSqlInsertForAcademic).collect(Collectors.toList());
        generatedSql.forEach(System.out::println);

    }

    private Pair<String, String> getFilterParam(int offset, int limit) {
        String filterValue = "{\"offset\":" + offset + ",\"limit\":" + limit + "}";
        String filterParamName = "filter";
        return new ImmutablePair<>(filterParamName, filterValue);
    }

    private String generateSqlInsertForAcademic(AcademicResponse academic) {
        StringBuilder bldr = new StringBuilder("INSERT INTO ")
                .append(ACADEMIC_TABLE)
                .append("(").append(ACADEMIC_COLUMNS).append(") ")
                .append(" VALUES (")
                .append("'").append(escapeQuotes(academic.getTeacher_id())).append("',")
                .append("'").append(escapeQuotes(academic.getTeacher_name())).append("',")
                .append("'").append(escapeQuotes(academic.getTeacher_full_name())).append("',")
                .append("'").append(escapeQuotes(academic.getTeacher_short_name())).append("'")
                .append(");");
        return bldr.toString();
    }

    private String escapeQuotes(String str) {
        return str.replace("'", "''");
    }

    private Set<Academic> loadCurrentAcademics() {
        return academicRep.findAll();
    }

}
